
var modal_icpc = document.getElementById('modal-icpc');
var modal_school = document.getElementById('modal-school');
var modal_university = document.getElementById('modal-university');
var modal_travels = document.getElementById('modal-travels');
var modal_ci = document.getElementById('modal-ci');
var modal_karanva = document.getElementById('modal-karanva');
var modal_usa = document.getElementById('modal-usa');
var icpc = document.getElementById("icpc-activate");
var school = document.getElementById("school-activate");
var university = document.getElementById("university-activate");
var travel = document.getElementById("travel-activate");
var ci = document.getElementById("ci-activate");
var karanva = document.getElementById("karanva-activate");
var usa = document.getElementById("usa-activate");
icpc.onclick = function() {
    modal_icpc.style.display = "block";
}
school.onclick = function() {
    modal_school.style.display = "block";
}
university.onclick = function() {
    modal_university.style.display = "block";
}
travel.onclick = function() {
    modal_travels.style.display = "block";
}
ci.onclick = function() {
    modal_ci.style.display = "block";
}
karanva.onclick = function() {
    modal_karanva.style.display = "block";
}
usa.onclick = function() {
    modal_usa.style.display = "block";
}
window.onclick = function(event) {
    switch (event.target) {
      case modal_icpc:
        modal_icpc.style.display = "none";
      break;
      case modal_school:
        modal_school.style.display = "none";
      break;
      case modal_university:
        modal_university.style.display = "none";
      break;
      case modal_travels:
        modal_travels.style.display = "none";
      break;
      case modal_ci:
        modal_ci.style.display = "none";
      break;
      case modal_karanva:
        modal_karanva.style.display = "none";
      break;
      case modal_usa:
        modal_usa.style.display = "none";
      break;
    }
}
